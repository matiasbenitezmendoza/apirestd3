//Mensajes bot de Telegram

const TelegramBot = require('node-telegram-bot-api');
const token = '975118403:AAGBEHS1hBjLFNijreSA9klsYZCXLSJxGcQ';
const bot = new TelegramBot(token, {polling: true});
const idchat = "-350838554";

let telegram = {};

let getCadenaMsj = (url, method) =>{
    var msj = "";
    if(method == "GET"){
                switch ( url ) {
                    case "/users":
                        msj = "[Busqueda de usuarios]";
                        break;
                    case "/login":
                        msj = "[Inicio de Sesión]";
                        break;
                    case "/roles":
                        msj = "[Consulta de Roles]";
                        break;
                    case "/rolesid":
                            msj = "[Consulta de Rol]";
                            break;
                    default:
                        msj = url;
                        break;
                }
    }
    if(method == "POST"){
                switch ( url ) {
                    case "/users":
                        msj = "[Registro de nuevo Usuario]";
                        break;
                    case "/roles":
                            msj = "[Registro de nuevo Rol]";
                            break;
                    default:
                        msj = "[]";
                        break;
                }
    }
    if(method == "PUT"){
        switch ( url ) {
            case "/users":
                msj = "[Actualización de Usuario]";
                break;
            case "/roles":
                    msj = "[Actualización de Rol]";
                    break;
            default:
                msj = "[]";
                break;
        }
   }
   if(method == "DELETE"){
    switch ( url ) {
        case "/users":
            msj = "[Eliminación de Usuario]";
            break;
        case "/roles":
                msj = "[Eliminación de Rol]";
                break;
        default:
            msj = "[]";
            break;
    }
}
    return  msj;

};


telegram.peticion = (url, user, method) =>{
    var fecha =  new Date();
    var cadena_fecha = fecha.toString().substring(0, 33);
    var mensaje = "Dashboard "+getCadenaMsj(url, method)+" ["+user.usuario+"] ["+cadena_fecha+"]";
    bot.sendMessage(idchat, mensaje);
};
 
telegram.error = (url, user, method, error) =>{
    var fecha =  new Date();
    var cadena_fecha = fecha.toString().substring(0, 33);
    var mensaje = "Error ["+error+"]"+getCadenaMsj(url, method)+" ["+user.usuario+"] ["+cadena_fecha+"]";
    bot.sendMessage(idchat, mensaje);
};

telegram.errorAutenticacion = (url, error, method) =>{
    var fecha =  new Date();
    var cadena_fecha = fecha.toString().substring(0, 33);
    var mensaje = "Error de Autenticación "+getCadenaMsj(url, method)+" ["+error+"] ["+cadena_fecha+"]";
    bot.sendMessage(idchat, mensaje);
};

module.exports = telegram;