var jwt = require('jsonwebtoken');
const SEED = process.env.SEED || 'dashboardminionssadecv'; // SED ES UNA SEMILLA SECRETA EJEMP: semilla-biensecreta
var telegram = require('./telegram.middleware');

/**
 * Verifica token
**/

exports.verificaToken = function(req, res, next) {

   /*
    const bearerHeader = req.headers['authorization'];
    if(typeof bearerHeader !== 'undefined'){

    }
    else{
         res.sendStatus(403);
    }
   */
   
    // si no trae nada en la cabecera
    if (!req.headers.authorization) {
        telegram.errorAutenticacion(req.url, "Token vacio", req.method);
        return res.status(401).json({
            status: false,
            msj: "Error de Autenticación.",
            data: []
        });
    }
    
    var token;

    if (req.headers.authorization.split(" ").length > 0) {
        // la cabecera de authorization 'Bearer $token        
        token = req.headers.authorization.split(" ")[1];// separa el token ['Bearer', $token]        
    } else {
        telegram.errorAutenticacion(req.url, "Token no valido", req.method);
        return res.status(401).json({
            status: false,
            msj: "Error de Autenticación.",
            data: []
        });
    }

    // verifica el token con nuestra semilla
    jwt.verify(token, SEED, (err, decoded) => {
        if (err) {
            telegram.errorAutenticacion(req.url, err, req.method);
            return res.status(401).json({
                status: false,
                msj: 'Sesión Caducada ...',
                data: []
            });
        }
        // se agrega los datos de usuario a request para obtener los datos en las rutas
        req.user = decoded.usuario; 
        next();
    });

}