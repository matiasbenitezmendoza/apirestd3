//GuardarImagenes
var fs = require('fs');
var path = require('path');

let imagen = {};

imagen.save = (req) =>{
    var file_name = 'Imagen no subida...';

    if(!req.files){
        return "default.png";
    }

    if(req.files.imagen == undefined){
        return "default.png";
    }
    // Conseguir nombre y la extensión del archivo
    var file_path = req.files.imagen.path;
    var file_split = file_path.split('\\');

    // * ADVERTENCIA * EN LINUX O MAC
    // var file_split = file_path.split('/');

    // Nombre del archivo
    var file_name = file_split[3];

    // Extensión del fichero
    var extension_split = file_name.split('\.');
    var file_ext = extension_split[1];
    // Comprobar la extension, solo imagenes, si es valida borrar el fichero
    if(file_ext != 'png' && file_ext != 'jpg' && file_ext != 'jpeg'){
        // borrar el archivo subido
        fs.unlink(file_path, (err) => {
            return false;
        });
        return false;
    }
    else{
         // Si todo es valido retornar el nombre de la imagen
         return file_name;
    }   
    return false;
};
 

module.exports = imagen;