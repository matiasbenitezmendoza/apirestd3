'use strict'
const controller = require('../controllers/controller_imagen');
const app = require('express').Router();
var authentification = require('../middlewares/autenticacion.middleware');

app.get('/imagen', controller.getImagen);

module.exports = app;

