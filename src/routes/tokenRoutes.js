const app = require('express').Router();
var authentification = require('../middlewares/autenticacion.middleware');

var jwt = require('jsonwebtoken');
var semilla = 'dashboardminionssadecv';

//constante user (se puede cambiar la informacion de la constante segun desee)
const user = {
    id: 1,
    nombre: "matias",
    apellido_paterno: "b",
    apellido_materno: "m",
    usuario: "matias.benitez@sspo.com"
};


//get (crear token)
app.get('/token',  (req, res) =>{        
     jwt.sign({user}, semilla, (err, data) => {
     res.status(200).json(data);
     });
});


//post (crear token con tiempo de expiracion)
app.post('/token',  (req, res) =>{  
         const datos = req.body;    
         if(datos["tiempo"] === undefined || Number.isInteger(datos["tiempo"]) === false){
              res.status(200).json({
                                     "Error": "Parametro tiempo incorrecto o vacio"
                                   });
         }
         var tiempo = datos["tiempo"] + 's'; 
         jwt.sign({user}, semilla, { expiresIn: tiempo}, (err, data) => {
         res.status(200).json(data);
         });
});

module.exports = app;
