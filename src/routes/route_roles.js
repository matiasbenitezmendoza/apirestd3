'use strict'
const controller = require('../controllers/controller_roles');
const app = require('express').Router();
var authentification = require('../middlewares/autenticacion.middleware');

app.get('/roles', authentification.verificaToken, controller.getRoles );
app.get('/rolesid', authentification.verificaToken, controller.getRol );
app.post('/roles', authentification.verificaToken, controller.postRoles);
app.put('/roles', authentification.verificaToken, controller.putRoles);
app.delete('/roles', authentification.verificaToken, controller.deleteRoles);


module.exports = app;

