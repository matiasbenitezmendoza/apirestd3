'use strict'
const controller = require('../controllers/controller_user');
const app = require('express').Router();
var authentification = require('../middlewares/autenticacion.middleware');
var jwt = require('jsonwebtoken');
var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: "./src/img/user"});
/*
app.get('/tab_usuarios', (req, res) =>{     
         // bot.sendMessage("-350838554", 'Inicio de sesion Dashboard');
      modelo.get({}, (err, data) => {
            res.status(200).json(data);
       });
});
*/

app.get('/users', authentification.verificaToken, controller.getUsuarios );
app.post('/users', md_upload, authentification.verificaToken, controller.postUsuario);
app.get('/usersid', authentification.verificaToken, controller.getUsuario );
app.put('/users', md_upload, authentification.verificaToken, controller.putUsuario);
//app.post('/usersupdate', md_upload, authentification.verificaToken, controller.putUsuario);
app.delete('/users', authentification.verificaToken, controller.deleteUsuario);
app.put('/userstema', authentification.verificaToken, controller.putTema);

app.get('/login', controller.getLogin);
app.get('/rutas', controller.getRutas);

module.exports = app;

