const db = require('../data/db_dashboard');
var md5 = require('md5');

let model = {};

model.get = (data, callback) =>{
  
   var sql = "SELECT * from tab_usuarios WHERE status = 1";             
     
   db.getConnection(function(err, connection) {
     if(err) throw err;
     connection.query(sql, function(err, rows) {
        if(err) {  
            connection.release();
            console.error(err);  
            callback(true, err.sqlMessage);                 
            return;
        }
        connection.release();
        callback(null, rows);
    });
  });    
};

model.getId = (data, callback) =>{
  
  var sql = "SELECT * from tab_usuarios WHERE id_usuario = ? AND status = 1";             
    
  db.getConnection(function(err, connection) {
    if(err) throw err;
    connection.query(sql,[data.id], function(err, rows) {
       if(err) {  
           connection.release();
           console.error(err);  
           callback(true, err.sqlMessage);                 
           return;
       }
       connection.release();
       callback(null, rows);
   });
 });    
};

model.insert = (data, callback) =>{
     
    var sql = "SELECT * from tab_usuarios WHERE usuario = ? AND status = 1";             
    
    /*comprobar que el usuario no exista*/
    db.getConnection(function(err, connection) {
        if(err) throw err;
        connection.query(sql, [data.usuario],
            function(err, rows) {
                if(err) {  
                    connection.release();
                    console.error(err);  
                    callback(true, err.sqlMessage);                 
                    return;
                }
                if(rows.length > 0){
                    connection.release();
                    console.error(err);  
                    callback(true, "No se puede agregar el usuario '"+data.usuario+"', el usuario ya existe");                 
                    return;
                }

                data.password = md5(data.password);
                data.status = 1;
                data.fecha = new Date();
                connection.query( 'INSERT INTO tab_usuarios SET ?', data, function(err, rows) {
                  if(err) {  
                    connection.release();
                    callback(true, err.sqlMessage); 
                    return;
                  }
                  connection.release();
                  callback(null, rows);
            });


              }
        );

        
    });
    
    /*Encriptrar password 
    data["password"] = md5(data["password"]);
    
    /*Insertar nuevo usuario
    db.getConnection(function(err, connection) {
          if(err) throw err;
          connection.query( 'INSERT INTO tab_usuarios SET ?', data, function(err, rows) {
                if(err) {  
                  connection.release();
                  callback(true, err.sqlMessage); 
                  return;
                }
                connection.release();
                callback(null, {'insertId': "Registro Insertado"});
          });
    });*/

};

model.login = (usuario, password, callback) =>{
  
  var pass = md5(password);
  var sql = "SELECT * from tab_usuarios INNER JOIN tab_roles ON tab_roles.id_rol = tab_usuarios.tipo "+
            "WHERE tab_usuarios.usuario = ? AND tab_usuarios.password = ? AND tab_usuarios.status = 1";             
  
  db.getConnection(function(err, connection) {
    if(err) throw err;
    connection.query(sql, [usuario, pass],
      function(err, rows) {
          if(err) {  
              connection.release();
              console.error(err);  
              callback(true, err.sqlMessage);                 
              return;
          }
          connection.release();
          callback(null, rows);
   });
 });    
};

model.update = (data, callback) =>{
     
  var sql = "SELECT * from tab_usuarios WHERE id_usuario != ? AND usuario = ? AND status = 1";             
  
  /*comprobar que el usuario exista*/
  db.getConnection(function(err, connection) {
      if(err) throw err;
      connection.query(sql, [data.id_usuario, data.usuario],
          function(err, rows) {
              if(err) {  
                  connection.release();
                  console.error(err);  
                  callback(true, err.sqlMessage);                 
                  return;
              }
              if(rows.length > 0){
                  connection.release();
                  console.error(err);  
                  callback(true, "El usuario ya existe.");                 
                  return;
              }

              data.password = md5(data.password);
              connection.query( 'UPDATE tab_usuarios SET ? WHERE id_usuario = ?',[data, data.id_usuario], function(err, rows) {
                if(err) {  
                  connection.release();
                  callback(true, err.sqlMessage); 
                  return;
                }
                connection.release();
                callback(null, rows);
             });

            }
      );

      
  });
  

};


model.delete = (data, callback) =>{
     
  var sql = "SELECT * from tab_usuarios WHERE id_usuario = ? AND status = 1";             
  
  /*comprobar que el usuario exista*/
  db.getConnection(function(err, connection) {
      if(err) throw err;
      connection.query(sql, [data.id_usuario],
          function(err, rows) {
              if(err) {  
                  connection.release();
                  console.error(err);  
                  callback(true, err.sqlMessage);                 
                  return;
              }
              if(rows.length === 0){
                  connection.release();
                  console.error(err);  
                  callback(true, "No se encontro el usuario.");                 
                  return;
              }

              connection.query( 'UPDATE tab_usuarios SET status = 2 WHERE id_usuario = ?',[data.id_usuario], function(err, rows) {
                if(err) {  
                  connection.release();
                  callback(true, err.sqlMessage); 
                  return;
                }
                connection.release();
                callback(null, rows);
             });

            }
      );

      
  });
  
};

model.updateTema = (data, callback) =>{
     
  var sql = "SELECT * from tab_usuarios WHERE id_usuario = ? AND status = 1";             
  
  /*comprobar que el usuario exista*/
  db.getConnection(function(err, connection) {
      if(err) throw err;
      connection.query(sql, [data.id_usuario],
          function(err, rows) {
              if(err) {  
                  connection.release();
                  console.error(err);  
                  callback(true, err.sqlMessage);                 
                  return;
              }
              if(rows.length === 0){
                  connection.release();
                  console.error(err);  
                  callback(true, "No se encontro el usuario.");                 
                  return;
              }

              connection.query( 'UPDATE tab_usuarios SET tema = ? WHERE id_usuario = ?',[data.tema, data.id_usuario], function(err, rows) {
                if(err) {  
                  connection.release();
                  callback(true, err.sqlMessage); 
                  return;
                }
                connection.release();
                callback(null, rows);
             });

            }
      );

      
  });
  
};



module.exports = model;