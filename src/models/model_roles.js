const db = require('../data/db_dashboard');

let model = {};

model.get = (data, callback) =>{
  
   var sql = "SELECT * from tab_roles WHERE status = 1";             
     
   db.getConnection(function(err, connection) {
     if(err) throw err;
     connection.query(sql, function(err, rows) {
        if(err) {  
            connection.release();
            console.error(err);  
            callback(true, err.sqlMessage);                 
            return;
        }
        connection.release();
        callback(null, rows);
    });
  });    
};

model.getId = (data, callback) =>{
  
  var sql = "SELECT * from tab_roles WHERE id_rol = ? AND status = 1";             
    
  db.getConnection(function(err, connection) {
    if(err) throw err;
    connection.query(sql,[data.id], function(err, rows) {
       if(err) {  
           connection.release();
           console.error(err);  
           callback(true, err.sqlMessage);                 
           return;
       }
       connection.release();
       callback(null, rows);
   });
 });    
};

model.insert = (data, callback) =>{
     
    data.status = 1;
    data.fecha = new Date();
    /*comprobar que el usuario no exista*/
    db.getConnection(function(err, connection) {
        if(err) throw err;
            connection.query( 'INSERT INTO tab_roles SET ?', data, function(err, rows) {
                  if(err) {  
                    connection.release();
                    callback(true, err.sqlMessage); 
                    return;
                  }
                  connection.release();
                  callback(null, rows);
            });
    });
    

};

model.update = (data, callback) =>{
     
  db.getConnection(function(err, connection) {
      if(err) throw err;
          connection.query( 'UPDATE tab_roles SET ? WHERE id_rol = ?',[data, data.id_rol], function(err, rows) {
                if(err) {  
                  connection.release();
                  callback(true, err.sqlMessage); 
                  return;
                }
                connection.release();
                callback(null, rows);
          });
  });
  

};

model.delete = (data, callback) =>{
     
  db.getConnection(function(err, connection) {
      if(err) throw err;
          connection.query( 'UPDATE tab_roles SET status = 2 WHERE id_rol = ?',[data.id_rol], function(err, rows) {
                if(err) {  
                  connection.release();
                  callback(true, err.sqlMessage); 
                  return;
                }
                connection.release();
                callback(null, rows);
          });
  });
  

};

 


module.exports = model;