//modulos de node
const express = require('express');
const jwt = require('jsonwebtoken');
var cors = require('cors');
const app = express();
const path = require("path");
const morgan = require('morgan');
const bodyParser = require('body-parser');

//rutas
const tokenRoutes = require('./routes/tokenRoutes');
const usuarios_routes = require('./routes/route_user');
const roles_routes = require('./routes/route_roles');
const img_routes = require('./routes/route_img');

//settings
app.set('port', process.env.PORT || 5000);
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

//middlewares
app.use(morgan('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false}));
app.use(cors());

//rutas con prefijo
app.use('/api',tokenRoutes);
app.use('/api',usuarios_routes);
app.use('/api',roles_routes);
app.use('/api',img_routes);

//prueba de servidor tiempo
//star server
app.listen(app.get('port'), ()=>{
  console.log('server on port', app.get('port'));
});
