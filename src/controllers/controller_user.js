'use strict'
const modelo = require('../models/model_user');
let response = require('../data/response');
var telegram = require('../middlewares/telegram.middleware');
var img = require('../middlewares/upload.middleware');
var jwt = require('jsonwebtoken');
var semilla = 'dashboardminionssadecv';
var tiempo_token = '46400s'; 


var validator = require('validator');

var controller = {
   
  getUsuarios: (req, res) => {
                                modelo.get({}, (err, data) => {
                                    if(err){
                                        telegram.error(req.url, req.user, req.method, data);
                                        response.status = false;
                                        response.msj = data;
                                        response.data = [];
                                        return res.status(500).json(response);
                                    }else{
                                        telegram.peticion(req.url, req.user, req.method);
                                        response.status = true;
                                        response.msj = "";
                                        response.data = data;
                                        return res.status(200).json(response);
                                    }
                                
                            });            
                            },
  postUsuario: (req, res) => {
                                var datos = req.body; 
                                try{
                                    var validator_nombre = !validator.isEmpty(datos.nombre);
                                    var validator_tipo = validator.isNumeric(datos.tipo+"");
                                    var validator_usuario = !validator.isEmpty(datos.usuario);
                                    var validator_password = !validator.isEmpty(datos.password);
                                    var validator_sexo = !validator.isEmpty(datos.sexo);

                                }
                                catch(error){
                                            telegram.error(req.url, req.user, req.method, "Parametros incompletos");
                                            response.status = false;
                                            response.msj = "Ocurrio un error, revise los parametros.";
                                            response.data = [];
                                            return res.status(400).json(response);
                                }
                              
                                var name_img = img.save(req);
                                if(name_img != false){
                                            datos.imagen = name_img;
                                }else{
                                            telegram.error(req.url, req.user, req.method, "Ocurrio un error al subir la imagen");
                                            response.status = false;
                                            response.msj = "Verifique que este mandando una imagen con el formato correcto.";
                                            response.data = [];
                                            return res.status(400).json(response);
                                }
                                

                                if(validator_nombre && validator_tipo  && validator_usuario  && validator_password && validator_sexo){
                                            modelo.insert(datos, (err, data) => {
                                                    if(err){
                                                        telegram.error(req.url, req.user, req.method, data);
                                                        response.status = false;
                                                        response.msj = data;
                                                        response.data = [];
                                                        return res.status(400).json(response);
                                                    }else{
                                                        telegram.peticion(req.url, req.user, req.method);
                                                        response.status = true;
                                                        response.msj = "Registro Agregado";
                                                        response.data = data;
                                                        return res.status(200).json(response);
                                                    }
                                            });
                                }
                                else{
                                            telegram.error(req.url, req.user, req.method, "Parametros incompletos");
                                            response.status = false;
                                            response.msj = "Parametro incompletos";
                                            response.data = datos;
                                            return res.status(400).json(response);
                                }
        
                                
                                     
                             },
  getLogin: (req, res) => {
                                var user = {
                                     "usuario" : req.ip
                                }
                                const datos = req.query
                                try{
                                    var validator_usuario = !validator.isEmpty(datos.usuario);
                                    var validator_password = !validator.isEmpty(datos.password);
                                }
                                catch(error){
                                            telegram.error(req.path, user, req.method, "Parametros incompletos");
                                            response.status = false;
                                            response.msj = "Ocurrio un error, revise los parametros.";
                                            response.data = [];
                                            return res.status(200).json(response);
                                }
                                if(validator_password && validator_usuario){
                                            modelo.login(datos.usuario, datos.password, (err, data) => {
                                                if(err){
                                                        telegram.error(req.path, user, req.method, data);
                                                        response.status = false;
                                                        response.msj = data;
                                                        response.data = [];
                                                        return res.status(200).json(response);
                                                }

                                                if(data.length > 0){
                                                            var usuario = data[0];
                                                            usuario.password = "";
                                                            jwt.sign({usuario}, semilla, { expiresIn: tiempo_token}, (err, token) => {
                                                                telegram.peticion(req.path, usuario, req.method);
                                                                response.status = true;
                                                                response.msj = "";
                                                                var datos_session = {
                                                                    "nombre" : usuario.nombre +" "+usuario.apellido_paterno,
                                                                    "usuario" : usuario.usuario,
                                                                    "id_usuario" : usuario.id_usuario,
                                                                    "sexo" : usuario.sexo,
                                                                    "imagen": usuario.imagen,
                                                                    "tema": usuario.tema,
                                                                    "permisos": usuario.permisos,
                                                                    "rutas": usuario.rutas,
                                                                    "token" : token
                                                                };
                                                                response.data = datos_session;
                                                                return res.status(200).json(response);
                                                            });                                                            
                                                }else{
                                                            telegram.error(req.path, user, req.method, "Intento de Sesión con : Usuario = "+datos.usuario+", Password = "+datos.password);
                                                            response.status = false;
                                                            response.msj = "Usuario o Contraseña incorrecta";
                                                            response.data = [];
                                                            return res.status(200).json(response);
                                                }
                                            });     
                                }   
                                else{
                                        telegram.error(req.path, user, req.method, "Parametros incompletos");
                                        response.status = false;
                                        response.msj = "Parametro incompletos";
                                        response.data = [];
                                        return res.status(200).json(response);
                                } 
                                   
                            },
  getRutas: (req, res) => {
                             var data =  [
                                            {
                                                name: 'Dashboard',
                                                url: '/dashboard',
                                                icon: 'icon-speedometer'
                                            },
                                            {
                                                name: 'Registro',
                                                url: '/registro',
                                                icon: 'icon-people'
                                            }
                                        ];
                                        response.status = true;
                                        response.msj = "";
                                        response.data = data;
                                        return res.status(200).json(response);
                                           
                            },
  getUsuario: (req, res) => {
                                const datos = req.query
                                try{
                                    var validator_id = !validator.isEmpty(datos.id);
                                }
                                catch(error){
                                            telegram.error(req.path, req.user, req.method, "Parametros incompletos");
                                            response.status = false;
                                            response.msj = "Ocurrio un error, revise los parametros.";
                                            response.data = [];
                                            return res.status(400).json(response);
                                }
                                if(validator_id){

                                        modelo.getId(datos, (err, data) => {
                                            if(err){
                                                telegram.error(req.url, req.user, req.method, data);
                                                response.status = false;
                                                response.msj = data;
                                                response.data = [];
                                                return res.status(400).json(response);
                                            }else{
                                                var objeto = {};
                                                if(data.length > 0){
                                                     objeto = data[0];
                                                }

                                                telegram.peticion(req.url, req.user, req.method);
                                                response.status = true;
                                                response.msj = "";
                                                response.data = objeto;
                                                return res.status(200).json(response);
                                            }
                                        });   
                                        
                                }   
                                else{
                                        telegram.error(req.path, req.user, req.method, "Parametros incompletos");
                                        response.status = false;
                                        response.msj = "Parametro incompletos";
                                        response.data = [];
                                        return res.status(400).json(response);
                                }
                                       
                           },
  putUsuario: (req, res) => {
                                var datos = req.body; 
                                try{
                                    var validator_id = validator.isNumeric(datos.id_usuario+"");
                                    var validator_nombre = !validator.isEmpty(datos.nombre);
                                    var validator_tipo = validator.isNumeric(datos.tipo+"");
                                    var validator_usuario = !validator.isEmpty(datos.usuario);
                                    var validator_password = !validator.isEmpty(datos.password);
                                    var validator_sexo = !validator.isEmpty(datos.sexo);
                                }
                                catch(error){
                                            telegram.error(req.url, req.user, req.method, "Parametros incompletos, "+error);
                                            response.status = false;
                                            response.msj = "Revise los parametros, "+error;
                                            response.data = datos;
                                            return res.status(400).json(response);
                                }

                                var name_img = img.save(req);
                                if(name_img != false){
                                          if(datos.imagen != undefined && datos.imagen !== ""){
                                                  datos.imagen = name_img;
                                          }
                                }else{
                                            telegram.error(req.url, req.user, req.method, "Ocurrio un error al subir la imagen");
                                            response.status = false;
                                            response.msj = "Verifique que este mandando una imagen con el formato correcto.";
                                            response.data = [];
                                            return res.status(400).json(response);
                                }
                                
                              
              
                                if(validator_id && validator_nombre  && validator_tipo  && validator_usuario && validator_password && validator_sexo){
                                            modelo.update(datos, (err, data) => {
                                                    if(err){
                                                        telegram.error(req.url, req.user, req.method, data);
                                                        response.status = false;
                                                        response.msj = data;
                                                        response.data = [];
                                                        return res.status(400).json(response);
                                                    }else{
                                                        telegram.peticion(req.url, req.user, req.method);
                                                        response.status = true;
                                                        response.msj = "Registro Actualizado";
                                                        response.data = data;
                                                        return res.status(200).json(response);
                                                    }
                                            });
                                }
                                else{
                                            telegram.error(req.url, req.user, req.method, "Parametros incompletos");
                                            response.status = false;
                                            response.msj = "Parametro incompletos";
                                            response.data = datos;
                                            return res.status(400).json(response);
                                }
        
                                
                                     
                          },
  deleteUsuario: (req, res) => {
        var datos = req.body; 
        try{
            var validator_id = validator.isNumeric(datos.id_usuario+"");
        }
        catch(error){
                    telegram.error(req.url, req.user, req.method, "Parametros incompletos, "+error);
                    response.status = false;
                    response.msj = "Revise los parametros,"+error;
                    response.data = datos;
                    return res.status(400).json(response);
        }
      
        if(validator_id){
                    modelo.delete(datos, (err, data) => {
                            if(err){
                                telegram.error(req.url, req.user, req.method, data);
                                response.status = false;
                                response.msj = data;
                                response.data = [];
                                return res.status(400).json(response);
                            }else{
                                telegram.peticion(req.url, req.user, req.method);
                                response.status = true;
                                response.msj = "Registro Eliminado";
                                response.data = data;
                                return res.status(200).json(response);
                            }
                    });
        }
        else{
                    telegram.error(req.url, req.user, req.method, "Parametros incompletos");
                    response.status = false;
                    response.msj = "Parametro incompletos";
                    response.data = datos;
                    return res.status(400).json(response);
        }       
                           },
  putTema: (req, res) => {
                            var datos = req.body; 
                            try{
                                var validator_id = validator.isNumeric(datos.id_usuario+"");
                                var validator_tema = !validator.isEmpty(datos.tema);
                            }
                            catch(error){
                                        telegram.error(req.url, req.user, req.method, "Parametros incompletos, "+error);
                                        response.status = false;
                                        response.msj = "Revise los parametros,"+error;
                                        response.data = datos;
                                        return res.status(400).json(response);
                            }
                          
                            if(validator_id && validator_tema){
                                        modelo.updateTema(datos, (err, data) => {
                                                if(err){
                                                    telegram.error(req.url, req.user, req.method, data);
                                                    response.status = false;
                                                    response.msj = data;
                                                    response.data = [];
                                                    return res.status(400).json(response);
                                                }else{
                                                    response.status = true;
                                                    response.msj = "Tema Actualizado";
                                                    response.data = data;
                                                    return res.status(200).json(response);
                                                }
                                        });
                            }
                            else{
                                        telegram.error(req.url, req.user, req.method, "Parametros incompletos");
                                        response.status = false;
                                        response.msj = "Parametro incompletos";
                                        response.data = datos;
                                        return res.status(400).json(response);
                            }       
                },
 
};

module.exports = controller;

