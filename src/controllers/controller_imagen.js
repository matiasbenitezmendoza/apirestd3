'use strict'
let response = require('../data/response');
var validator = require('validator');

var fs = require('fs');
var path = require('path');

var controller = {
   
    getImagen: (req, res) => {

        const datos = req.query
        try{
            var validator_nombre = !validator.isEmpty(datos.nombre);
        }
        catch(error){
                    response.status = false;
                    response.msj = "Ocurrio un error, revise los parametros.";
                    response.data = [];
                    return res.status(400).json(response);
        }
        if(validator_nombre){
                var path_name = "./src/img/user/"+ datos.nombre;      
                fs.exists(path_name, (exists) =>{
                    console.log(exists);
                    console.log(path_name);

                    if(exists){
                         return res.sendFile(path.resolve(path_name));
                    }else{
                        response.status = false;
                        response.msj = "La imagen no existe";
                        response.data = [];
                        return res.status(404).json(response);
                    }
                });
        }   
        else{
                response.status = false;
                response.msj = "Parametro incompletos";
                response.data = datos;
                return res.status(404).json(response);
        }        
    }
  
};

module.exports = controller;

