'use strict'
const modelo = require('../models/model_roles');
let response = require('../data/response');
var telegram = require('../middlewares/telegram.middleware');


var validator = require('validator');

var controller = {
   
    getRoles: (req, res) => {
                                modelo.get({}, (err, data) => {
                                    if(err){
                                        telegram.error(req.url, req.user, req.method, data);
                                        response.status = false;
                                        response.msj = data;
                                        response.data = [];
                                        return res.status(400).json(response);
                                    }else{
                                        telegram.peticion(req.url, req.user, req.method);
                                        response.status = true;
                                        response.msj = "";
                                        response.data = data;
                                        return res.status(200).json(response);
                                    }
                                
                            });            
    },
    getRol: (req, res) => {
                                const datos = req.query
                                try{
                                    var validator_id = !validator.isEmpty(datos.id);
                                }
                                catch(error){
                                            telegram.error(req.path, req.user, req.method, "Parametros incompletos");
                                            response.status = false;
                                            response.msj = "Ocurrio un error, revise los parametros.";
                                            response.data = [];
                                            return res.status(400).json(response);
                                }
                                if(validator_id){

                                        modelo.getId(datos, (err, data) => {
                                            if(err){
                                                telegram.error(req.url, req.user, req.method, data);
                                                response.status = false;
                                                response.msj = data;
                                                response.data = [];
                                                return res.status(400).json(response);
                                            }else{
                                                var objeto = {};
                                                if(data.length > 0){
                                                     objeto = data[0];
                                                }

                                                telegram.peticion(req.url, req.user, req.method);
                                                response.status = true;
                                                response.msj = "";
                                                response.data = objeto;
                                                return res.status(200).json(response);
                                            }
                                        });   
                                        
                                }   
                                else{
                                        telegram.error(req.path, req.user, req.method, "Parametros incompletos");
                                        response.status = false;
                                        response.msj = "Parametro incompletos";
                                        response.data = [];
                                        return res.status(400).json(response);
                                }
                                       
    },
    postRoles: (req, res) => {
                                var datos = req.body; 
                                try{
                                    var validator_nombre = !validator.isEmpty(datos.nombre);
                                    var validator_permisos = !validator.isEmpty(datos.permisos);
                                    var validator_rutas = !validator.isEmpty(datos.rutas);
                                }
                                catch(error){
                                            telegram.error(req.url, req.user, req.method, "Parametros incompletos");
                                            response.status = false;
                                            response.msj = "Ocurrio un error, revise los parametros.";
                                            response.data = [];
                                            return res.status(400).json(response);
                                }
                              
              
                                if(validator_nombre && validator_permisos  && validator_rutas){
                                            modelo.insert(datos, (err, data) => {
                                                    if(err){
                                                        telegram.error(req.url, req.user, req.method, data);
                                                        response.status = false;
                                                        response.msj = data;
                                                        response.data = [];
                                                        return res.status(400).json(response);
                                                    }else{
                                                        telegram.peticion(req.url, req.user, req.method);
                                                        response.status = true;
                                                        response.msj = "Registro Agregado";
                                                        response.data = data;
                                                        return res.status(200).json(response);
                                                    }
                                            });
                                }
                                else{
                                            telegram.error(req.url, req.user, req.method, "Parametros incompletos");
                                            response.status = false;
                                            response.msj = "Parametro incompletos";
                                            response.data = datos;
                                            return res.status(400).json(response);
                                }
        
                                
                                     
    },
    putRoles: (req, res) => {
                                var datos = req.body; 
                                try{
                                    var validator_id = validator.isNumeric(datos.id_rol+"");
                                    var validator_nombre = !validator.isEmpty(datos.nombre);
                                    var validator_permisos = !validator.isEmpty(datos.permisos);
                                    var validator_rutas = !validator.isEmpty(datos.rutas);
                                }
                                catch(error){
                                            telegram.error(req.url, req.user, req.method, "Parametros incompletos, "+error);
                                            response.status = false;
                                            response.msj = "Revise los parametros,"+error;
                                            response.data = datos;
                                            return res.status(400).json(response);
                                }
                              
              
                                if(validator_nombre && validator_permisos  && validator_rutas  && validator_id){
                                            modelo.update(datos, (err, data) => {
                                                    if(err){
                                                        telegram.error(req.url, req.user, req.method, data);
                                                        response.status = false;
                                                        response.msj = data;
                                                        response.data = [];
                                                        return res.status(400).json(response);
                                                    }else{
                                                        telegram.peticion(req.url, req.user, req.method);
                                                        response.status = true;
                                                        response.msj = "Registro Actualizado";
                                                        response.data = data;
                                                        return res.status(200).json(response);
                                                    }
                                            });
                                }
                                else{
                                            telegram.error(req.url, req.user, req.method, "Parametros incompletos");
                                            response.status = false;
                                            response.msj = "Parametro incompletos";
                                            response.data = datos;
                                            return res.status(400).json(response);
                                }
        
                                
                                     
    },
    deleteRoles: (req, res) => {
        var datos = req.body; 
        try{
            var validator_id = validator.isNumeric(datos.id_rol+"");
        }
        catch(error){
                    telegram.error(req.url, req.user, req.method, "Parametros incompletos, "+error);
                    response.status = false;
                    response.msj = "Revise los parametros,"+error;
                    response.data = datos;
                    return res.status(400).json(response);
        }
      
        if(validator_id){
                    modelo.delete(datos, (err, data) => {
                            if(err){
                                telegram.error(req.url, req.user, req.method, data);
                                response.status = false;
                                response.msj = data;
                                response.data = [];
                                return res.status(400).json(response);
                            }else{
                                telegram.peticion(req.url, req.user, req.method);
                                response.status = true;
                                response.msj = "Registro Eliminado";
                                response.data = data;
                                return res.status(200).json(response);
                            }
                    });
        }
        else{
                    telegram.error(req.url, req.user, req.method, "Parametros incompletos");
                    response.status = false;
                    response.msj = "Parametro incompletos";
                    response.data = datos;
                    return res.status(400).json(response);
        }       
    }
  
};

module.exports = controller;

